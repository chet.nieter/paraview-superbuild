# Make a dummy visitbridge project so that the user can enable or disable
# building paraview with bridge support
superbuild_add_dummy_project(visitbridge
  DEPENDS boost)
